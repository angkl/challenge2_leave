## Challenge
**Problem**:
Create a SQL query that accept @Period_Start and @Period_End to list down all the
employees and leave type that are absent during these periods.

## Tests
The data are stored in Bigquery and the code is hosted on App Engine  
The following takes start(2019-07-08), end(2019-07-12) as parameters. 

[https://leave-dot-akl-challenge.appspot.com/leave/employee?start=2019-07-08&end=2019-07-12](https://leave-dot-akl-challenge.appspot.com/leave/employee?start=2019-07-08&end=2019-07-12)

*Note that it might take a few seconds (due to cold start) to get the result on your first invocation.

## SQL
Please refer to the [sql](https://bitbucket.org/angkl/challenge2_leave/src/master/sql.txt) code.
