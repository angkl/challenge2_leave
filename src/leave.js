const {BigQuery} = require('@google-cloud/bigquery');
const config = require('./config')

module.exports ={
    queryEmployee : queryEmployee
}

async function queryEmployee(periodStart, periodEnd){
    if(config.isTest){
        process.env.GOOGLE_APPLICATION_CREDENTIALS = config.gac
    }
    const bigqueryClient = new BigQuery();

    const sqlQuery = `
    SELECT 
        employee_name,
        leave_type
    FROM \`akl-challenge.challenge2.LeaveCalender\` t
    WHERE (t.start between @period_start and @period_end)  
        or (t.end between @period_start and @period_end)
    `;

    const options = {
        query: sqlQuery,
        location: 'US',
        params: {period_start: periodStart, period_end: periodEnd},
    };

    const [rows] = await bigqueryClient.query(options);

    return rows
}