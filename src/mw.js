module.exports = {
    errHand : errHand,
    checkPara : checkPara
}

async function errHand(ctx, next){
    try{
        await next()
    }catch (err){
        const msg = `${err.message}`
        ctx.body = msg
        console.log(msg)
    }
}

async function checkPara(ctx,next){
    const reqPara = ctx.request.query
    
    if(reqPara.start == null){
        ctx.body = "Parameter 'start' cannot be empty"
        ctx.status = 422
    }
    else if(reqPara.end == null){
        ctx.body = "Parameter 'end' cannot be empty"
        ctx.status = 422
    }
    else{
        await next()
    }
}
