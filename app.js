const koa = require('koa')
const cors = require('koa2-cors');
const koa_router = require('koa-router')
const leave = require('./src/leave')
const app = new koa()
const router = new koa_router()
const mw = require('./src/mw')

router.get('/leave/employee', async ctx=> {
    ctx.body = await leave.queryEmployee(ctx.request.query.start, ctx.request.query.end)
})

const PORT = process.env.PORT || 8002;
app
    .use(cors())
    .use(mw.errHand)
    .use(mw.checkPara)
    .use(router.routes())
    .use(router.allowedMethods())
    .listen(PORT, () => {
      console.log(`App listening on port ${PORT}`);
      console.log('Press Ctrl+C to quit.');
});
